from bs4 import BeautifulSoup
import requests
from dataclasses import dataclass


@dataclass
class ReportContent:
    title: str
    time: str


class TrojmiastoScrapper:
    URL = 'https://www.trojmiasto.pl/raport/drogowy'

    def scrape_reports(self, amount=3)-> [ReportContent]:
        report_contents = []

        page = requests.get(self.URL)
        soup = BeautifulSoup(page.content, 'html.parser')
        reports = soup.find_all(attrs={
            'class': 'report-content-inner'
        }, limit=amount)

        for report in reports:
            title = report.h3.a.text.strip()
            time = report.find('p', class_='time').text
            report_contents.append(ReportContent(title=title, time=time))
        return report_contents

if __name__ == '__main__':
    scraper = TrojmiastoScrapper()
    reports = scraper.scrape_reports(amount=3)

    for report in reports:
        print(f"title: {report.title} | time: {report.time}")