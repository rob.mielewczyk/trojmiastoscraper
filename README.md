# About project
Quick project made in 10 min.
Every time I drive somewhere first thing I do is check trojmiasto.pl
if something happened on the road. 

This project automates it for me, since I can tell siri to check and show/tell me
those headlines. This request connects to my home server and sends back response to siri.
